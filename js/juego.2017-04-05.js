//var configGame = new configGame();

//var game = new Phaser.Game(configGame.getWidth(), configGame.getHeight(), Phaser.CANVAS, configGame.getSource(), {preload: preload, create: create, update: update, render: render });
var game = new configGame();

function preload() {
    game.setInitConfig();
}

var layer;
var player;

function create() {
    //Game setPhysics()
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    //Background
    var bg = new configBackground(game, 0, 0, configGameGetWidth(), configGameGetHeight());
    bg.setInitConfig();

    //Map
    var map = new configMap(game);
    map.setInitConfig();
    layer = map.configLayer();

    //Game gravity(x, y)
    game.physics.arcade.gravity.y = 0;
    
    //Dude
    player = new configDude(game);
    player.setInitConfig(game);

    //Game cameraFollow()
    game.camera.follow(player);
}

function update() {
    //Game collidePlayerLayer()
    game.physics.arcade.collide(player, layer);
}

function render() {
    //game.debug.text(game.time.physicsElapsed, 32, 32);
    game.debug.body(player); // Hitbox
    //game.debug.bodyInfo(player, 16, 24);
    
    //  Un-comment this on to see the collision tiles
    // layer.debug = true;
}