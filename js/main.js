var game = new configGame();

function preload() {
    game.initPreload();
}

var layer;
var player;

function create() {
    //Background
    var bg = new configBackground(game, 0, 0, configGameGetWidth(), configGameGetHeight());
    bg.initCreate();

    //Map
    var map = new configMap(game);
    map.initCreate();
    layer = map.configLayer();

    //Dude
    player = new configDude(game);
    player.initCreate(game);
    
    //Game
    game.initCreate(player);
}

function update() {
    //Game
    game.setCollidePlayerLayer(player, layer);
}

function render() {
    //game.debug.text(game.time.physicsElapsed, 32, 32);
    //game.debug.body(player); // Hitbox
    //game.debug.bodyInfo(player, 16, 24);
    //player.body.blocked.up;
    
    //layer.debug = true; // tiles
}