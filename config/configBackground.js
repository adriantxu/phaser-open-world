var configBackground = function(_game, _x, _y, _width, _height, _key, _frame, bgMoveRange, bgMoveAmount) {
    _x = _x || 0;
    _y = _y || 0;
    _key = configBackgroundGetBackgroundKey(_key);
    _frame = _frame || null;

    //Configuracion exacta para crear el nuevo objeto
    Phaser.TileSprite.call(this, _game, _x, _y, _width, _height, _key, _frame);
    
    //Configuracion extendida del objeto
    this.bgMoveRange = bgMoveRange || 200;
    this.bgMoveRangeOriginal = this.bgMoveRange;
    this.bgMoveAmount = bgMoveAmount || 0.5;
    this.isBgDirectionRight = 1;
    
    _game.add.existing(this);
}

configBackground.prototype = Object.create(Phaser.TileSprite.prototype);
configBackground.prototype.constructor = configBackground;

configBackground.prototype.initCreate = function() {
    this.fixCamera();
}

configBackground.prototype.update = function() {
    this.moveTilePosition();
}

configBackground.prototype.fixCamera = function(param1) {
    param1 = param1 || true;
    this.fixedToCamera = param1;
}

configBackground.prototype.moveTilePosition = function() {
    if (this.bgMoveRange == this.bgMoveRangeOriginal) {
        this.bgDirectionRight = 0;
        this.bgMoveRange--;
    } else if (this.bgMoveRange == 0) {
        this.bgDirectionRight = 1;
        this.bgMoveRange++;
    } else {
        if (this.bgDirectionRight) {
            this.tilePosition.x += 0.5;
            this.bgMoveRange++;
        } else {
            this.tilePosition.x -= 0.5;
            this.bgMoveRange--;
        }
    }
}

// Funciones que necesitan estar fuera del objeto
var configBackgroundGetBackgroundKey = function(param1) { 
    return param1 || 'background';
}
var configBackgroundGetBackgroundUrl = function(param1) { 
    return param1 || 'assets/bg.gif';
}