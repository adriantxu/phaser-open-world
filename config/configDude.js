//https://phaser.io/docs/2.6.2/Phaser.Sprite.html
var configDude = function(_game, _x, _y, _key, _frame, offsetX, offsetY, bounceX, bounceY, enablePhysics, collideWorldBounds, facing, jumpTimer, jumpHeight, jumpTime) {
    _x = _x || 0;
    _y = _y || 0;
    _key = configDudeGetSpriteSheetName(_key);
    _frame = _frame || 18;
    offsetX = offsetX || 37;
    offsetY = offsetY || 15;
    
    //Configuracion exacta para crear el nuevo objeto
    Phaser.Sprite.call(this, _game, _x, _y, _key, _frame);
    
    collideWorldBounds = collideWorldBounds || true;
    bounceX = bounceX || 0;
    bounceY = bounceY || 0;
    this.enablePhysics = enablePhysics || true;
    this.enablePhysicsPlayer(_game);
    
    this.body.bounce.y = bounceY;
    this.body.collideWorldBounds = collideWorldBounds;
    this.body.setSize(configDudeGetWidth(), configDudeGetHeight(), offsetX, offsetY);
    
    this.facing = facing || 'right';
    this.facingAux = this.facing;
    
    this.jumpTimer = jumpTimer || 0;
    this.jumpHeight = jumpHeight || -550;
    this.jumpTime = jumpTime || 750;
    
    this.moveSpeed = 150;
    this.actions = {};
    
    _game.add.existing(this);
}

configDude.prototype = Object.create(Phaser.Sprite.prototype);
configDude.prototype.constructor = configDude;

configDude.prototype.initCreate = function(game) {
    this.setAnimations();
    this.setPlayerActions(game);
}

configDude.prototype.update = function() {
    this.setVelocity();
    this.checkPlayerActions();
    this.showPlayerInfo();
}

configDude.prototype.enablePhysicsPlayer = function(game) {
    if(this.enablePhysics) {
        game.physics.enable(this, Phaser.Physics.ARCADE);
    }
}

configDude.prototype.getWidth = function() {
    return this.width;
}

configDude.prototype.getHeight = function() {
    return this.height;
}

configDude.prototype.getSpriteSheetUrl = function() {
    return this.spriteSheetUrl;
}

configDude.prototype.getSpriteSheetName = function() {
    return this.spriteSheetName;
}

configDude.prototype.getBounceY = function() {
    return this.body.bounce.y;
}

configDude.prototype.getBounceX = function() {
    return this.body.bounce.x;
}

configDude.prototype.setVelocity = function(param1, param2) {
    this.setVelocityX(param1);
    this.setVelocityY(param2);
}

configDude.prototype.setVelocityY = function(param1) {
    this.body.velocity.y = param1 || 0;
}

configDude.prototype.setVelocityX = function(param1) {
    this.body.velocity.x = param1 || 0;
}

configDude.prototype.setMoveSpeed = function(param1) {
    this.moveSpeed = param1 || 0;
}

configDude.prototype.setAnimations = function() {
    var actions = {
        'directions': [{name: 'right'}, {name: 'left'}],
        'actions': [
            {name: 'Idle', frames: 10, duration: 10, loop: true},
            ////{name: 'Walk', frames: 10, duration: 10, loop: true},
            {name: 'Run', frames: 8, duration: 10, loop: true},
            //{name: 'Melee', frames: 7, duration: 10, loop: true},
            //{name: 'Ranged', frames: 3, duration: 10, loop: true},
            //{name: 'Hurt', frames: 10, duration: 10, loop: true},
            //{name: 'Die', frames: 10, duration: 10, loop: true},
            ////{name: 'Dash', frames: 10, duration: 10, loop: true},
            ////{name: 'Jump', frames: 10, duration: 10, loop: true}
        ]
    }
    
    var animations;
    var contFrames = 0;
    for (var contActions in actions.actions) {
        for (var contDirections in actions.directions) {
            animations = [];
            for(var i = 0; i < actions.actions[contActions].frames; i++) {
                animations.push(i + contFrames);
            }
            
            contFrames += actions.actions[contActions].frames;
            
            this.animations.add(actions.directions[contDirections].name + actions.actions[contActions].name, animations, actions.actions[contActions].duration, actions.actions[contActions].loop);
        }
    }
}

configDude.prototype.setPlayerActions = function(game) {
    this.actions['up'] = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.actions['left'] = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.actions['down'] = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.actions['right'] = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.actions['melee'] = game.input.activePointer.leftButton;
    this.actions['jump'] = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
}

configDude.prototype.checkPlayerActions = function(game) {
    //Movimiento
    if (this.actions['left'].isDown) {
        this.setVelocityX(this.moveSpeed * -1);

        this.animations.play(this.facing + 'Run')
        this.facingAux = this.facing = 'left';
        
        if (this.actions['up'].isDown) {
            this.setVelocityY(this.moveSpeed * -1);
        } else if (this.actions['down'].isDown) {
            this.setVelocityY(this.moveSpeed);
        }
    } else if (this.actions['right'].isDown) {
        this.setVelocityX(this.moveSpeed);

        this.animations.play(this.facing + 'Run')
        this.facingAux = this.facing = 'right';
        
        if (this.actions['up'].isDown) {
            this.setVelocityY(this.moveSpeed * -1);
        } else if (this.actions['down'].isDown) {
            this.setVelocityY(this.moveSpeed);
        }
    } else if (this.actions['up'].isDown) {
        this.setVelocityY(this.moveSpeed * -1);
        //El personaje no tiene animacion para ir arriba
        this.animations.play(this.facingAux);
    } else if (this.actions['down'].isDown) {
        this.setVelocityY(this.moveSpeed);
        //El personaje no tiene animacion para ir abajo
        this.animations.play(this.facingAux);
    } else {
        //this.animations.stop();
        //this.frame = 4;
        
        this.animations.play(this.facing + 'Idle')
        this.facing = 'idle';
    }
    
    //Melee
    if (this.actions['melee'].isDown) {
        this.animations.play('melee');
    }
   
    //Salto
    if (this.actions['jump'].isDown && this.body.onFloor() && game.time.now > this.jumpTimer) {
        this.body.velocity.y = this.jumpHeight;
        this.jumpTimer = game.time.now + this.jumpTime;
        this.animations.play('jump');
    }
    
    configDude.prototype.showPlayerInfo = function(game) {
        
    }
}

// Funciones que necesitan estar fuera del objeto
var configDudeGetSpriteSheetUrl = function(param1) { 
    return param1 || 'assets/dude.png';
}
var configDudeGetSpriteSheetName = function(param1) { 
    return param1 || 'dude';
}
var configDudeGetWidth = function(param1) {
    return param1 || 61;
}
var configDudeGetHeight = function(param1) {
    return param1 || 117;
}
var configDudeGetSpriteWidth = function(param1) {
    return param1 || 150;
}
var configDudeGetSpriteHeight = function(param1) {
    return param1 || 150;
}