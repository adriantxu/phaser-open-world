//http://phaser.io/docs/2.6.2/Phaser.Tilemap.html
var configMap = function(_game, _key, _tileWidth, _tileHeight, _width, _height) {
    _key = configMapGetKey(_key);
    _tileWidth = _tileWidth || 32;
    _tileHeight = _tileHeight || 32;
    _width = _width || 10;
    _height = _height || 10;
    
    //Configuracion exacta para crear el nuevo objeto
    Phaser.Tilemap.call(this, _game, _key, _tileWidth, _tileHeight, _width, _height);
}

configMap.prototype = Object.create(Phaser.Tilemap.prototype);
configMap.prototype.constructor = configMap;

configMap.prototype.initCreate = function() {
    this.setTilesetImg();
    this.setCollisionExclusion();
}

configMap.prototype.setTilesetImg = function(param1) {
    param1 = configMapGetMapKey(param1);
    
    this.addTilesetImage(param1);
}

configMap.prototype.setCollisionExclusion = function(param1) {
    this.setCollisionByExclusion([2, 3, 4, 5, 6]);
}

configMap.prototype.configLayer = function(param1) {
    param1 = configMapGetLayerKey(param1);
    
    var layer = this.createLayer(param1);
    layer.resizeWorld();
    
    return layer;
}

// Funciones que necesitan estar fuera del objeto
var configMapGetLayerKey = function(param1) { 
    return param1 || 'Tile Layer 1';
}
var configMapGetKey = function(param1) { 
    return param1 || 'level1';
}
var configMapGetMapKey = function(param1) { 
    return param1 || 'tiles-1';
}
var configMapGetMapUrl = function(param1) { 
    return param1 || 'assets/' + configMapGetMapName();
}
var configMapGetMapName = function(param1) { 
    return param1 || 'map.png';
}
var configMapGetJsonTileMap = function() {
    var mapData = configMapCreateJsonTileMap();
    //var tileData = [];
    
    return {
        "height": mapData.mapH,
        "layers": [{
            "data": mapData.tileArray,
            "height": mapData.mapH,
            "name": configMapGetLayerKey(),
            "opacity": 1,
            "type": "tilelayer",
            "visible": true,
            "width": mapData.mapW,
            "x": 0,
            "y": 0
        }],
        "orientation": "orthogonal",
        "properties": {
        
        },
        "tileheight": 64,
        "tilesets": [{
            "firstgid": 1,
            "image": configMapGetMapName(),
            "imageheight": 64,
            "imagewidth": 384,
            "margin": 0,
            "name": configMapGetMapKey(),
            "properties": {
            
            },
            "spacing": 0,
            "tileheight": 64,
            "tilewidth": 64
        }],
        "tilewidth": 64,
        "version": 1,
        "width": mapData.mapW
    };
}

var configMapCreateJsonTileMap = function() {
    //BIOMES
    var mapWidth = 135;
    var mapheight = 64;
    var voronoiArray = new Array();
    var randomBiome = 0;
    //Si repetimos los biomes hay mas probabilidad de que aparezcan, haciendo posible crear biomes raros
    //http://www.bioenciclopedia.com/biomas/
    var biomes = [
        //{ id: 2, nombre: 'pradera', w: 5, h: 5 },
        //{ id: 2, nombre: 'pradera', w: 5, h: 5 },
        //{ id: 2, nombre: 'pradera', w: 5, h: 5 },
        { id: 2, nombre: 'pradera', w: 5, h: 5 },
        { id: 3, nombre: 'bosque', w: 5, h: 5 },
        { id: 4, nombre: 'manglar', w: 5, h: 5 },
        { id: 5, nombre: 'colina', w: 5, h: 5 },
        { id: 6, nombre: 'desierto', w: 5, h: 5 }
    ];
    var randomnessParam = biomes.length;
    
    //Generamos el mapa "Voronoi"
    for (var i = 0; i < mapWidth * mapheight; i++) {
        if(typeof voronoiArray[i] === "undefined") {
            randomBiome = Math.floor(Math.random() * randomnessParam);
            
            var biomeW = biomes[randomBiome].w;
            var biomeH = biomes[randomBiome].h;
            
            var biomeI = i + biomeW - 1;
            biomeI = biomeI + (mapWidth * (biomeH - 1));
            
            
            for(var h = 0; h < biomeH; h++) {
                for(var w = 0; w < biomeW; w++) {
                        voronoiArray[biomeI] = biomes[randomBiome].id;
                    biomeI--;
                }
                
                biomeI += biomeW;
                biomeI -= mapWidth;
            }
        }
    }
    
    //MAPA
    var tileArray = new Array();
    var probability = 0;
    var probabilityModifier = 0;
    var landMassAmount = 1; // scale of 1 to 5
    var landMassSize = 5; // scale of 1 to 5
    var rndm = 0;
    var probabilityParam = 15;
    var rndmParam = 101;
    
    //Creamos el mapa con el agua y todo
    for (var i = 0; i < mapWidth * mapheight; i++) {
        probability = 0;
        probabilityModifier = 0;

        if (i < (mapWidth * 2) || i % mapWidth < 2 || i % mapWidth > (mapWidth - 3) || i > (mapWidth * mapheight) - ((mapWidth * 2) + 1)) {
            // make the edges of the map water
            probability = 0;
        } else {
            probability = probabilityParam + landMassAmount;

            if (i > (mapWidth * 2) + 2) {
                // Conform the tile upwards and to the left to its surroundings 
                var conformity =
                    (tileArray[i - mapWidth - 1] == (tileArray[i - (mapWidth * 2) - 1])) +
                    (tileArray[i - mapWidth - 1] == (tileArray[i - mapWidth])) +
                    (tileArray[i - mapWidth - 1] == (tileArray[i - 1])) +
                    (tileArray[i - mapWidth - 1] == (tileArray[i - mapWidth - 2]));

                if (conformity < 2) {
                    tileArray[i - mapWidth - 1] = !tileArray[i - mapWidth - 1];
                }
            }

            // get the probability of what type of tile this would be based on its surroundings 
            probabilityModifier = (tileArray[i - 1] + tileArray[i - mapWidth] + tileArray[i - mapWidth + 1]) * (19 + (landMassSize * 1.4));
        }

        rndm = (Math.random() * rndmParam);
        tileArray[i] = (rndm < (probability + probabilityModifier));
    }

    //En este punto ya tenemos el mapa voronoi, y el mapa irregular, que sera el mapa final cuando lo fusionemos con el voronoi
    for (var i = 0; i < tileArray.length; i++) {
        if (tileArray[i] === false) {
            //Agua
            tileArray[i] = 1;
        } else {
            tileArray[i] = voronoiArray[i];
        }
    }
    
    return {'tileArray': tileArray, 'mapW': mapWidth, 'mapH': mapheight};
}