var configGame = function(_width, _height, _source, _preloadFunc, _createFunc, _updateFunc, _renderFunc) {
    _width = configGameGetWidth(_width);
    _height = configGameGetHeight(_height);
    _source = _source || 'game';
    var _funcs = {
        preload: _preloadFunc || preload,
        create: _createFunc || create,
        update: _updateFunc || update,
        render: _renderFunc || render
    };
    
    
    //Configuracion exacta para crear el nuevo objeto
    Phaser.Game.call(this, _width, _height, Phaser.CANVAS, _source, _funcs);
}

configGame.prototype = Object.create(Phaser.Game.prototype);
configGame.prototype.constructor = configGame;

configGame.prototype.initPreload = function() {
    //Map
    this.load.tilemap(configMapGetKey(), null, configMapGetJsonTileMap(), Phaser.Tilemap.TILED_JSON);
    this.load.image(configMapGetMapKey(), configMapGetMapUrl());
    //Dude
    this.load.spritesheet(configDudeGetSpriteSheetName(), configDudeGetSpriteSheetUrl(), configDudeGetSpriteWidth(), configDudeGetSpriteHeight()); //Como ultimo valor se puede pasar un entero que hace referencia al numero total de frames que hay, si el png ocupa el 100% no hace falta poner un valor aqui, pero si termina con un frame transparente hay que ponerlo
    //Background
    this.load.image(configBackgroundGetBackgroundKey(), configBackgroundGetBackgroundUrl());
}

configGame.prototype.initCreate = function(player) {
    this.setGravity();
    this.setPhysicsArcade();
    this.setCameraFollow(player);
}
configGame.prototype.setGravity = function(param1, param2) {
    this.setGravityX(param1);
    this.setGravityY(param2);
}
configGame.prototype.setGravityX = function(param1) {
    this.physics.arcade.gravity.x = param1 || 0;
}
configGame.prototype.setGravityY = function(param1) {
    this.physics.arcade.gravity.y = param1 || 0;
}
configGame.prototype.setPhysicsArcade = function() {
    this.physics.startSystem(Phaser.Physics.ARCADE);
}
configGame.prototype.setCameraFollow = function(player) {
    this.camera.follow(player);
}
configGame.prototype.setCollidePlayerLayer = function(player, layer) {
    this.physics.arcade.collide(player, layer);
}
    
// Funciones que necesitan estar fuera del objeto
var configGameGetWidth = function(param1) {
    return param1 || 800;
}
var configGameGetHeight = function(param1) {
    return param1 || 600;
}