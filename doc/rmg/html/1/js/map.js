var nTerrainTypes = 13;
var terrainOrderStrength = 4;
var gameReady = false;
var mapWGlobal = 36;
var mapHGlobal = 36;
var winSizeGlobal = 11; //parseInt(mapWGlobal/6.67);
var winHSizeGlobal = 5; //parseInt(mapWGlobal/6.67);

function TerrainType(index, name, rep, style, matrix, passable) {
    this.index = index;
    this.name = name;
    this.rep = rep;
    this.style = style;
    this.matrix = matrix; //Elementos que puede tener vecinos? Porque se repiten los valores? Que significa: el numero maximo de esos vecinos que puede tener?
    this.passable = passable;
}

var terrain = new Array();
terrain.push(new TerrainType(-1, null, null, null, [], null)); //El primer valor del array no sale a la hora de crear el mapa. Hay que agregar la 1 posicion con null
terrain.push(new TerrainType(0, "VOID", "Void", "void", [1, 2, 3], false));
terrain.push(new TerrainType(1, "PLAINS", "Plains", "plains", [1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 5, 6, 6, 8, 8], true));
terrain.push(new TerrainType(2, "HILLS", "Hills", "hills", [2, 2, 2, 1, 1, 5, 4, 3, 3, 3, 3, 7], true));
terrain.push(new TerrainType(3, "MOUNTAIN", "Mount", "mountain", [2, 2, 3, 3, 3, 3, 11], true));
terrain.push(new TerrainType(4, "FOREST", "Forest", "forest", [2, 5, 5, 5, 4, 4, 4, 4], true));
terrain.push(new TerrainType(5, "BRUSH", "Brush", "brush", [5, 4, 2, 1], true));
terrain.push(new TerrainType(6, "SWAMP", "Swamp", "swamp", [10, 6, 6, 6, 1, 1], true));
terrain.push(new TerrainType(7, "DESERT", "Desert", "desert", [2, 3, 7, 7, 7, 7, 7], true));
terrain.push(new TerrainType(8, "SHORE", "Shore", "shore", [6, 8, 9, 9], true));
terrain.push(new TerrainType(9, "OCEAN", "Ocean", "ocean", [8, 9, 9, 9, 12], false));
terrain.push(new TerrainType(10, "LAKE", "Lake", "lake", [8, 6, 10, 10], false));
terrain.push(new TerrainType(11, "PEAK", "Peak", "peaks", [11, 3, 3, 3], false));
terrain.push(new TerrainType(12, "Octopus Spawning Grounds", "Octopus", "abyss", [9], false));
terrain.push(new TerrainType(13, "SETTLEMENT", "Settle", "settlement", [1], true));

function Tile() {
    this.genCheck = false;
    this.genIndex = 500000;
    this.terrain = 0;
}
var map = new Array();
var mapW = mapWGlobal;
var mapH = mapHGlobal;

//Aqui se crea todo el mapa
for (hc = 0; hc < mapH; hc++) {
    for (wc = 0; wc < mapW; wc++) {
        map.push(new Tile());
    }
}


var mapComplete = false;
var ic = 0;
var icMax = 50000; //Just making sure the loop finishes at some point
var center = mapW * mapH / 2 + mapW / 2;

map[center].genIndex = 0;
map[center].terrain = 13;
/*
 
 var seed2=Math.floor(Math.random(map.length));
 
 map[seed2].terrain=1;
 
 map[seed2].genIndex=0;
 
 
 
 var seed3=Math.floor(Math.random(map.length));
 
 map[seed3].terrain=9;
 
 map[seed3].genIndex=0;
 
 */
var nextTile = -1;

while (!mapComplete && ic < icMax) {
    ic++;
    if (nextTile != -1) {
        w = nextTile % mapW;
        h = Math.floor(nextTile / mapW);
        var c = 1;
        var ter = map[nextTile].terrain;
        //north
        northRef = (mapW) * (h - 1) + w;
        if (h <= 0) {
            northRef = (mapW) * (mapH - 1) + w;
        }
        SetMap(northRef, c, ter);
        c = Math.floor(Math.random() * terrainOrderStrength);
        //east
        eastRef = (mapW) * h + w + 1;
        if (!(w < mapW - 1)) {
            eastRef = (mapW) * h;
        }
        SetMap(eastRef, c, ter);
        c = Math.floor(Math.random() * terrainOrderStrength);
        //south
        southRef = (mapW) * (h + 1) + w;
        if (!(h < mapH - 1)) {
            southRef = w;
        }
        SetMap(southRef, c, ter);
        c = Math.floor(Math.random() * terrainOrderStrength);
        //west
        westRef = (mapW) * h + w - 1;
        if (!(w > 0)) {
            westRef = (mapW) * h + mapW - 1;
        }
        SetMap(westRef, c, ter);
        c = Math.floor(Math.random() * terrainOrderStrength);
        map[nextTile].genCheck = true;
        nextTile = -1;
    }
    nextTile = GetNextTile();
    if (nextTile == -1) {
        if (nextTile == -1) {
            mapComplete = true;
        }
    }
}

function SetMap(ref, gI, ter) {
    if (ref < map.length) {
        if (!map[ref].genCheck && (map[ref].terrain == 0)) {
            map[ref].terrain = GenTerrain(ter);
            map[ref].genIndex = map[nextTile].genIndex + gI;
        }
    }
    else {
        document.write('out of bounds');
    }
}

function GenTerrain(from) {
    var to = 1;
    t = terrain[from];
    s = t.matrix.length;
    to = t.matrix[Math.floor(Math.random() * s)];
    return to;

}

function DrawMap() {
    for (hc = -winSize; hc <= winSize; hc++) {
        //document.write('<br>');
        for (wc = -winSize; wc <= winSize; wc++) {
            var c = (mapW) * (hc + cY) + wc + cX;
            if (hc == 0 && wc == 0) {
                StatusMessage(terrain[map[c].terrain].name);
            }
            else if ((wc + cX) > 0 && (wc + cX) < mapW && (hc + cY) > 0 && (hc + cY) < mapH) {
                var l = document.getElementById(wc + 'by' + hc);
                l.className = terrain[map[c].terrain].style;
                l.innerHTML = terrain[map[c].terrain].rep;
                //Aqui se crea el span que hace de tile
                //Y si metemos aqui un for, de 0 a un numero aleatorio entre 1 y X para crear un biome irregular de este tipo?
            }
            else {
                var l = document.getElementById(wc + 'by' + hc);
                l.className = "mapEdge";
            }
        }
    }
    //var c=(mapW)*(cY)+cX;
    //StatusMessage(terrain[map[c].terrain].name);
}

function StatusMessage(text) {
    var s = document.getElementById("status");
    s.innerHTML = text;
}

function SetDrawMap() {
    var content = "contenido";
    
    for (hc = -winSize; hc <= winSize; hc++) {
        document.getElementById(content).innerHTML+= '<br />';
        for (wc = -winSize; wc <= winSize; wc++) {
            document.getElementById(content).innerHTML+= '<span id="' + wc + 'by' + hc + '" class="player">PLAYER</span>';
        }
    }
    document.getElementById(content).innerHTML+= '<br /><span id="status"></span>';
}

function GetNextTile() {
    var nt = -1;
    var nv = 100000;
    for (cc = 0; cc < map.length; cc++) {
        if (map[cc].genIndex < nv && (!map[cc].genCheck || map[cc].terrain == 0)) {
            nt = cc;
            nv = map[cc].genIndex;
        }
    }
    return nt;
}

var cX = Math.floor(mapW / 2);
var cY = Math.floor(mapH / 2);
var nX = 0;
var nY = 0;
var winSize = winSizeGlobal;
var cycleDelay = 500;
SetDrawMap();
DrawMap();
gameReady = true;
