window.onload = function() {
    init();
}

//Si repetimos los biomes hay mas probabilidad de que aparezcan, haciendo posible crear biomes raros
var biomes = [
    { id: 0, nombre: 'pradera' },
    { id: 0, nombre: 'pradera' },
    { id: 0, nombre: 'pradera' },
    { id: 0, nombre: 'pradera' },
    { id: 1, nombre: 'bosque', },
    { id: 2, nombre: 'cemento' },
    { id: 3, nombre: 'colina' },
    { id: 4, nombre: 'pradera' }
];

var randomnessParam = biomes.length;

function init() {
    var mainContainer = document.getElementById("mainContainer");
    var xTileSize = 10;
    var yTileSize = 10;
    var random;
    var clear;

    for (var m = 0; m < yTileSize; m++) {
        for (var n = 0; n < xTileSize; n++) {
            clear = (!n) ? 'clear' : '';
            random = randomFunction();
            mainContainer.innerHTML += '<span class="tile' + biomes[random].id + ' ' + clear + '"></span>'
        }
    }
}

var randomFunction = function() {
    return Math.floor(Math.random() * randomnessParam);
}
