window.onload = function() {
    reload();
}

var imageSize = 256;
var imageSizeToScale = 100;

var xOffset = scaleToSize(148);
var yOffset = scaleToSize(29);
var zOffset = scaleToSize(148);

var xIsometricOffset = scaleToSize(105);
var yIsometricOffset = scaleToSize(24);

var imageNames = ["./img/1.png", "./img/2.png", "./img/3.png", "./img/4.png", "./img/5.png"];

var xTileSize = 10;
var yTileSize = 10;
var zTileSize = 1;

var generationMethod = 1;
var randomnessParam = imageNames.length;

/*
Alphabet:
	A -> rotate right
	B -> rotate left
	C -> up
	D -> down
	E -> go 
	F -> brush up/down
	G -> change texture left
	H -> change texture right
*/
var aRule = "afbeghg";
var bRule = "aefgd";
var cRule = "bfgced";
var dRule = "cchfgeea";
var eRule = "acgaef";
var fRule = "aeeeff";
var gRule = "acgahef";
var hRule = "aeeefghf";
//L-System seed
var seed = "abcfefggdeedh";
var numberOfIterations = 1;

function changeSize() {
    xTileSize = document.getElementById("tilesX").value;
    yTileSize = document.getElementById("tilesY").value;
    zTileSize = document.getElementById("tilesZ").value;
    randomnessParam = document.getElementById("randomnessParam").value;
    reload();
}

function changeRules() {
    aRule = document.getElementById("ruleA").value;
    bRule = document.getElementById("ruleB").value;
    cRule = document.getElementById("ruleC").value;
    dRule = document.getElementById("ruleD").value;
    eRule = document.getElementById("ruleE").value;
    fRule = document.getElementById("ruleF").value;
    gRule = document.getElementById("ruleG").value;
    hRule = document.getElementById("ruleH").value;
    seed = document.getElementById("seed").value;
    numberOfIterations = document.getElementById("numberOfIterations").value;
    reload();
}

function setMethod(meth) {
    generationMethod = meth;
}

function reload() {
    var mainContainer = document.getElementById("mainContainer");
    //remove all elements before
    while (mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.firstChild);
    }
    //check which one method is selected
    if (generationMethod == 0) {
        mapGenerationLoop(mainContainer, proceduralFunction);
    }
    else if (generationMethod == 1) {
        mapGenerationLoop(mainContainer, randomFunction);
    }
    else if (generationMethod == 2) {
        lSystemGenerationMethod(mainContainer)
    }
}

function lSystemGenerationMethod(mainContainer) {
    var seedAfterTransmormation = lIteration(seed, numberOfIterations);

    paintMap(mainContainer, seedAfterTransmormation);
}

function paintMap(mainContainer, seedAfterTransmormation) {
    //start point of the brush
    var minX = 0;
    var minY = 0;
    var brushX = 10;
    var brushY = 10;
    var brushZ = -10;
    var direction = 0;
    var color = 0;
    var isBrushPainting = true;
    while (seedAfterTransmormation.length != 0) {
        var ctrl = seedAfterTransmormation.charAt(0);
        if (ctrl == 'a') {
            direction++;
            if (direction > 3) {
                direction = 0;
            }
        }
        else if (ctrl == 'b') {
            direction--;
            if (direction < 0) {
                direction = 3;
            }
        }
        else if (ctrl == 'c') {
            brushZ++;
        }
        else if (ctrl == 'd') {
            brushZ--;
        }
        else if (ctrl == 'e') {
            //go
            if (direction == 0) {
                brushX++;
            }
            else if (direction == 1) {
                brushY--;
            }
            else if (direction == 2) {
                brushX--;
            }
            else if (direction == 3) {
                brushY++;
            }
            if (isBrushPainting) {
                drawImageOnSpecifiedTilePosition(color, brushX, brushY, brushZ, mainContainer);
                var xAbsolute = (xOffset * xTileSize) - (brushX + 1) * xOffset + (brushY * xIsometricOffset);
                if (minX > xAbsolute) {
                    minX = xAbsolute;
                }
                var yAbsolute = brushY * yOffset + (brushX * yIsometricOffset) + (zTileSize * zOffset) - (brushZ + 1) * zOffset;
                if (minY > yAbsolute) {
                    minY = yAbsolute;
                }
            }
        }
        else if (ctrl == 'f') {
            isBrushPainting = !isBrushPainting;
        }
        else if (ctrl == 'g') {
            color++;
            if (color >= imageNames.length) {
                color = 0;
            }
        }
        else if (ctrl == 'h') {
            color--;
            if (color < 0) {
                color = 2;
            }
        }

        seedAfterTransmormation = seedAfterTransmormation.substring(1);
    }

    mainContainer.style.left = -minX;
    mainContainer.style.top = -minY;
}


function lIteration(seedToIterate, iterations) {
    var tempSeed = seedToIterate;
    var newSeed = "";
    while (tempSeed.length != 0) {
        newSeed += transformChar(tempSeed.charAt(0));
        tempSeed = tempSeed.substring(1);
    }
    iterations--;
    if (iterations > 0) {
        newSeed = lIteration(newSeed, iterations);
    }
    return newSeed;
}

function transformChar(character) {
    if (character == 'a') {
        return aRule;
    }
    else if (character == 'b') {
        return bRule;
    }
    else if (character == 'c') {
        return cRule;
    }
    else if (character == 'd') {
        return dRule;
    }
    else if (character == 'e') {
        return eRule;
    }
    else if (character == 'f') {
        return fRule;
    }
    else if (character == 'g') {
        return gRule;
    }
    else if (character == 'h') {
        return hRule;
    }
    else {
        return "";
    }
}


function scaleToSize(paramToResize) {
    return paramToResize * imageSizeToScale / imageSize;
}

function mapGenerationLoop(mainContainer, generationFunction) {
    for (var h = 0; h < zTileSize; h++) {
        for (var m = 0; m < yTileSize; m++) {
            for (var n = 0; n < xTileSize; n++) {
                drawImageOnSpecifiedTilePosition(generationFunction(m, n, h), n, m, h, mainContainer)
            }
        }
    }
}
var proceduralFunction = function(m, n, h) {
    return (m + n + h) % randomnessParam;
}

var randomFunction = function() {
    return Math.floor(Math.random() * randomnessParam);
}


function setCoords(x, y, img) {
    img.style.position = "absolute";
    img.style.left = x;
    img.style.top = y;
    img.style.width = imageSizeToScale;
}

function drawImageOnSpecifiedTilePosition(whichOne, xTile, yTile, zTile, whereToAppend) {
    drawImageOnSpecifiedPosition(whichOne,
        (xOffset * xTileSize) - (xTile + 1) * xOffset + (yTile * xIsometricOffset),
        yTile * yOffset + (xTile * yIsometricOffset) + (zTileSize * zOffset) - (zTile + 1) * zOffset,
        mainContainer);
}

function drawImageOnSpecifiedPosition(whichOne, x, y, whereToAppend) {
    if (whichOne >= 0 && whichOne < imageNames.length) {
        var imageToCreate = document.createElement("img");
        imageToCreate.src = imageNames[whichOne];
        setCoords(x, y, imageToCreate);
        whereToAppend.appendChild(imageToCreate);
    }
}
