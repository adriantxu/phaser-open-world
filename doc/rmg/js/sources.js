var totalSources = 16;
var sourceList = [];

for(i = 0; i <= totalSources; i++) {
    sourceList[i] = [];
}

var estados = [];
estados.push('Descartado');
estados.push('Pendiente');
estados.push('Interesante pero no');
estados.push('Interesante - Revisar');
estados.push('Dungeon generator');

var Descartado = 0;
var Pendiente = 1;
var InteresantePeroNo = 2;
var Interesante = 3;
var Dungeon = 4;

sourceList[1].push({directorio: '1', puntuacion: '6', estado: estados[Interesante], url: 'https://foro.elhacker.net/programacion_general/javascript_crear_un_mapa_alaeatorio_es_para_un_juego-t365536.0.html'});
sourceList[1].push({directorio: '1.1', puntuacion: '6', estado: estados[Interesante], url: 'http://sirisian.com/javascriptgame/tests/valuenoise2.html'});
sourceList[5].push({directorio: '5.1', puntuacion: '6', estado: estados[Interesante], url: 'https://github.com/rjanicek/voronoi-map-haxe'});
sourceList[5].push({directorio: '5.2', puntuacion: '?', estado: estados[Interesante], url: 'https://github.com/rjanicek/voronoi-map-js'});
sourceList[5].push({directorio: '5.3', puntuacion: '0', estado: estados[InteresantePeroNo], url: 'https://github.com/kylepixel/terrain-generator'});
sourceList[5].push({directorio: '5.4', puntuacion: '0', estado: estados[InteresantePeroNo], url: 'https://github.com/feelic/PtolemyJS'});
sourceList[5].push({directorio: '5.5', puntuacion: '0', estado: estados[InteresantePeroNo], url: 'https://github.com/Evelios/procIsland'});
sourceList[6].push({directorio: '6', puntuacion: '0', estado: estados[Pendiente], url: 'http://www.redblobgames.com/maps/terrain-from-noise/'});
sourceList[8].push({directorio: '8', puntuacion: '6', estado: estados[Interesante], url: 'http://jsfiddle.net/AyexeM/zMZ9y/'});
sourceList[9].push({directorio: '9', puntuacion: '6', estado: estados[Interesante], url: 'https://github.com/cuberite/cuberite/blob/master/docs/Generator.html'});
sourceList[10].push({directorio: '10', puntuacion: '7', estado: estados[Interesante] + '. Video', url: 'http://www.youtube.com/watch?v=QSP9wsYIGBw'});
sourceList[11].push({directorio: '11', puntuacion: '7', estado: estados[Interesante], url: 'http://lebesnec.github.io/island.js/'});
sourceList[12].push({directorio: '12', puntuacion: '7', estado: estados[Interesante], url: 'https://plnkr.co/edit/zqzQIzDf0L3qz7XBni5l?p=preview'});
sourceList[16].push({directorio: '', puntuacion: '?', estado: estados[Pendiente], url: 'http://www.kamiro.com/canvashex.cfm'});
sourceList[16].push({directorio: '', puntuacion: '?', estado: estados[Pendiente], url: 'https://jsfiddle.net/ssLqwLj6/13/'});
sourceList[16].push({directorio: '', puntuacion: '0', estado: estados[Dungeon], url: 'https://gamedevelopment.tutsplus.com/tutorials/create-a-procedurally-generated-dungeon-cave-system--gamedev-10099'});
sourceList[16].push({directorio: '', puntuacion: '0', estado: estados[Dungeon], url: 'https://jsfiddle.net/bigbadwaffle/YeazH/'});
sourceList[16].push({directorio: '', puntuacion: '0', estado: estados[Dungeon], url: 'https://gamedevacademy.org/how-to-procedurally-generate-a-dungeon-in-phaser-part-1/'});
sourceList[16].push({directorio: '', puntuacion: '0', estado: estados[Dungeon], url: 'http://perplexingtech.weebly.com/game-dev-blog/a-random-dungeon-generator-for-phaserjs'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://libnoise.sourceforge.net/tutorials/tutorial5.html'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://devmag.org.za/2009/04/25/perlin-noise/'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://www.bluh.org/code-the-diamond-square-algorithm/'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://gamedev.stackexchange.com/questions/31241/random-map-generation'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://gamedev.stackexchange.com/questions/82941/procedurally-generated-top-view-2d-rpg-map-generation'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://gamedev.stackexchange.com/questions/35743/how-do-i-randomly-generate-a-top-down-2d-level-with-separate-sections-and-is-inf'});
sourceList[16].push({directorio: '', puntuacion: '-1', estado: estados[Descartado], url: 'http://bigbadwofl.me/random-dungeon-generator/'});

function setSources() {
    var srcList = '<ul class="lista-centrada m-b-15">';
    var srcSubList = [];
    var srcSubListTotal = 0;
    var url = window.location.href;
    var destacado = '';

    for (i = 1; i <= totalSources; i++) {
        srcSubList = sourceList[i];
        destacado = (url.search('/' + i + '/') == -1) ? '' : 'destacado';
        
        srcSubListTotal = (typeof srcSubList != 'undefined') ? srcSubList.length : 0;
        
        srcList += (srcSubListTotal) ? '<li class="' + destacado + '">' : '';
        
        for (j = 0; j < srcSubListTotal; j++) {
            srcList += '<ul>';
            srcList += '<li><a href="/openworldWConfig/doc/rmg/html/' + srcSubList[j].directorio + '/index.html" title="' + srcSubList[j].directorio + '">Demo ' + srcSubList[j].directorio + '</a>';
            srcList += '<li>Puntuación: ' + srcSubList[j].puntuacion + '</li>';
            srcList += '<li>Estado: ' + srcSubList[j].estado + '</li>';
            srcList += '<li>Url: <a href="' + srcSubList[j].url + '" title="URL" target="_blank">URL</a></li>';
            srcList += '</ul>';
        }
        
        srcList += (srcSubListTotal) ? '</li>' : '';
    }

    srcList += '</ul>';

    document.getElementById("sources").innerHTML = srcList;
    
    mainJS();
}

window.onload = setSources();
